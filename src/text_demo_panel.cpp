#include "text_demo_panel.hpp"

TextDemoPanel::TextDemoPanel(const bool &id)
    : index(0)
{
  std::cout << "Create TextDemoPanel instance ..." << std::endl;
  if (id)
    this->initUIs();
}

void TextDemoPanel::initUIs()
{
  this->text_edit = new QLineEdit();
  this->text_browser = new QTextBrowser();
  this->setLayout(ORIGIN_BOX());
}

QFont TextDemoPanel::set_font()
{
  QFont font("Arial", 36, QFont::Bold);
  return font;
}

QVBoxLayout *TextDemoPanel::ORIGIN_BOX()
{
  // 初始化垂直布局
  QVBoxLayout *vbox = new QVBoxLayout();

  // 添加一个面板到布局中
  vbox->addWidget(text_browser);
  vbox->addWidget(PANEL_1_());

  connect(this->text_edit, &QLineEdit::returnPressed, this, &TextDemoPanel::UPDATE_TEXT_SLOTS);
  // vbox->addStretch(1);
  
  // 返回配置好的布局
  return vbox;
}

QWidget *TextDemoPanel::PANEL_1_()
{
  this->text_browser->setMinimumSize(600, 400);
  this->text_browser->setReadOnly(true);
  this->text_browser->setAcceptRichText(true);
  this->text_browser->setOpenExternalLinks(true);
  this->text_browser->setStyleSheet("background-color: rgb(0, 0, 0);color:green;"); // CSS 样式
  this->text_browser->setFont(set_font());

  QHBoxLayout *hbox = new QHBoxLayout();
  QWidget* panel = new QGroupBox("文本输入");

  text_edit->setMinimumWidth(300);
  hbox->addWidget(text_edit);
  hbox->addWidget(SELECT_UPDATE_TEXT_BUTTON());
  hbox->addStretch(1);

  panel->setLayout(hbox);
  
  return panel;
}

QPushButton* TextDemoPanel::SELECT_UPDATE_TEXT_BUTTON()
{
  auto Btn = new QPushButton();
  Btn->setText("选择图像");
  this->ADD_UPDATE_TEXT_SLOTS(Btn);
  return Btn;
}

void TextDemoPanel::ADD_UPDATE_TEXT_SLOTS(QPushButton *Btn)
{
  connect(Btn, &QPushButton::clicked, this, &TextDemoPanel::UPDATE_TEXT_SLOTS);
}

void TextDemoPanel::UPDATE_TEXT_SLOTS()
{
  auto text_content = this->text_edit->text();
  
  this->text_browser->append(text_content);
  this->text_edit->clear();
}


