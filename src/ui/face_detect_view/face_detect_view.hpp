/* 
face_detect_view.hpp
 */
#pragma once

#include "QWidget"
#include "QLabel"
#include "QLineEdit"
#include "QCheckBox"
#include "QDoubleSpinBox"
#include "QRadioButton"

class FaceDetectView : public QWidget
{
  Q_OBJECT
public:
  FaceDetectView();
  void initUIs();
public slots:
  void select_image();
  void do_face_detect();
  void select_config_file();
  void select_weight_file();

private:
  QLabel *image_label{nullptr};

  QLineEdit *weight_file_edit{nullptr};
  QLineEdit *config_file_edit{nullptr};

  QCheckBox *show_fps_chk{nullptr};
  QCheckBox *show_score_chk{nullptr};
  QDoubleSpinBox *score_spinbox{nullptr};

  QRadioButton *img_file_rbt{nullptr};
  QRadioButton *video_file_rbt{nullptr};
  QLineEdit *data_file_edit{nullptr};
};
