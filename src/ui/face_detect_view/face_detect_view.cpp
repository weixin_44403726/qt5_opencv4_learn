#include "face_detect_view.hpp"

#include <iostream>

#include "QVBoxLayout"
#include "QHBoxLayout"
#include "QPushButton"
#include "QFileDialog"
#include "QGroupBox"
#include "QMessageBox"

#pragma execution_character_set("utf-8")

FaceDetectView::FaceDetectView()
{
  std::cout << "create MyCustomPanel instance..." << std::endl;
  initUIs();
}

void FaceDetectView::initUIs()
{
  QPixmap pixmap("D:/images/lena.jpg");
  this->image_label = new QLabel();
  image_label->setAlignment(Qt::AlignCenter);
  image_label->setStyleSheet("background-color:rgb(0, 0, 0);color:red");
  image_label->setPixmap(pixmap);
  image_label->setFixedSize(800, 550);
  this->data_file_edit = new QLineEdit();
  this->data_file_edit->setEnabled(false);
  QPushButton *selectBtn = new QPushButton("选择图像");

  img_file_rbt = new QRadioButton("图像文件");
  video_file_rbt = new QRadioButton("视频文件");
  img_file_rbt->setChecked(true);
  QHBoxLayout *box1 = new QHBoxLayout();
  QWidget *panel1 = new QGroupBox("数据");
  box1->addWidget(img_file_rbt);
  box1->addWidget(video_file_rbt);
  box1->addWidget(this->data_file_edit);
  box1->addWidget(selectBtn);
  panel1->setLayout(box1);

  weight_file_edit = new QLineEdit();
  config_file_edit = new QLineEdit();
  weight_file_edit->setEnabled(false);
  config_file_edit->setEnabled(false);
  auto *weight_btn = new QPushButton("选择...");
  auto *config_btn = new QPushButton("选择...");
  QHBoxLayout *box2 = new QHBoxLayout();
  QWidget *panel2 = new QGroupBox("模型");
  box2->addWidget(new QLabel("权重文件"));
  box2->addWidget(weight_file_edit);
  box2->addWidget(weight_btn);
  box2->addWidget(new QLabel("配置文件"));
  box2->addWidget(config_file_edit);
  box2->addWidget(config_btn);
  panel2->setLayout(box2);

  show_fps_chk = new QCheckBox("FPS");
  show_score_chk = new QCheckBox("置信度");
  show_fps_chk->setChecked(true);
  show_score_chk->setChecked(true);
  score_spinbox = new QDoubleSpinBox();
  score_spinbox->setRange(0, 1);
  score_spinbox->setSingleStep(0.01);
  score_spinbox->setValue(0.5);
  QHBoxLayout *box3 = new QHBoxLayout();
  QWidget *panel3 = new QGroupBox("显示");
  box3->addWidget(show_fps_chk);
  box3->addWidget(show_score_chk);
  box3->addWidget(new QLabel("得分: "));
  box3->addWidget(score_spinbox);
  panel3->setLayout(box3);

  QHBoxLayout *hbox4 = new QHBoxLayout();
  QWidget *panel4 = new QWidget();
  auto *applyBtn = new QPushButton("运行");
  hbox4->addStretch(1);
  hbox4->addWidget(applyBtn);
  panel4->setLayout(hbox4);

  QVBoxLayout *vbox = new QVBoxLayout();
  vbox->addWidget(panel1);
  vbox->addWidget(panel2);
  vbox->addWidget(panel3);
  vbox->addWidget(panel4);
  vbox->addWidget(image_label);
  vbox->addStretch(1);
  this->setLayout(vbox);

  // add listener
  connect(selectBtn, SIGNAL(clicked()), this, SLOT(select_image()));
  connect(weight_btn, SIGNAL(clicked()), this, SLOT(select_weight_file()));
  connect(config_btn, SIGNAL(clicked()), this, SLOT(select_config_file()));
  connect(applyBtn, SIGNAL(clicked()), this, SLOT(do_face_detect()));
}

void FaceDetectView::select_config_file()
{
  std::cout << "button clicked ...." << std::endl;
  auto fileName = QFileDialog::getOpenFileName(this, "选择模型配置文件", "/home", tr("Config(*.pbtxt);"));
  if (fileName.isEmpty())
  {
    return;
  }
  this->config_file_edit->setText(fileName);
}

void FaceDetectView::select_weight_file()
{
  std::cout << "button clicked ...." << std::endl;
  auto fileName = QFileDialog::getOpenFileName(this, "选择模型权重文件", "/home", tr("Weights(*.pb);"));
  if (fileName.isEmpty())
  {
    return;
  }
  this->weight_file_edit->setText(fileName);
}

void FaceDetectView::do_face_detect()
{
  auto weight_file = this->weight_file_edit->text();
  auto config_file = this->config_file_edit->text();
  float t_score = this->score_spinbox->value();
  bool show_fps = this->show_fps_chk->isChecked();
  bool show_score = this->show_score_chk->isChecked();
  auto data_file = this->data_file_edit->text();

  auto ret = QMessageBox::question(this, "选择", "你确定要运行人脸检测吗?", QMessageBox::StandardButtons(QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No));
  if (ret == QMessageBox::StandardButton::No)
  {
    std::cout << "dddddddddddddddddd" << std::endl;
    return;
  }

  if (weight_file.isEmpty() || config_file.isEmpty() || data_file.isEmpty())
  {
    QMessageBox::warning(this, "警告", "模型或者数据未设置...");
    return;
  }
  QMessageBox::information(this, "信息", "人脸检测运行成功...");
}

void FaceDetectView::select_image()
{
  std::cout << "button clicked ...." << std::endl;
  if (this->img_file_rbt->isChecked())
  {
    auto fileName = QFileDialog::getOpenFileName(this, "Open Image", "/home", tr("Images(*.png *.jpg);"));
    if (fileName.isEmpty())
    {
      return;
    }
    this->data_file_edit->setText(fileName);
    QPixmap pixmap(fileName);
    pixmap = pixmap.scaled(QSize(800, 550), Qt::KeepAspectRatio);
    this->image_label->setPixmap(pixmap);
  }
  if (this->video_file_rbt->isChecked())
  {
    auto fileName = QFileDialog::getOpenFileName(this, "Open Image", "/home", tr("Videos(*.mp4);"));
    if (fileName.isEmpty())
    {
      return;
    }
    this->data_file_edit->setText(fileName);
  }
}
