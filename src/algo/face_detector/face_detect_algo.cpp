#include "face_detect_algo.hpp"

#pragma execution_character_set("utf-8")

FaceDetectAlgo::FaceDetectAlgo(std::string weight_file,
               std::string config_file,
               float t_score,
               bool show_fps,
               bool show_score)
  : t_score(0.25), show_fps(true), show_score(true)
{
  this->net = cv::dnn::readNetFromTensorflow(weight_file, config_file);
  this->t_score = t_score;
  this->show_fps = show_fps;
  this->show_score = show_score;
}

void FaceDetectAlgo::initConfig()
{

}

void FaceDetectAlgo::infer_frame(cv::Mat &frame)
{
  
} 
