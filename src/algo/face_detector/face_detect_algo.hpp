/*
face_detect_algo.hpp
 */

#pragma once
#include "opencv2/opencv.hpp"
#include "opencv2/dnn.hpp"

class FaceDetectAlgo
{
public:
  FaceDetectAlgo(std::string weight_file,
                 std::string config_file,
                 float t_score,
                 bool show_fps = true,
                 bool show_score = true);

  void initConfig();
  void infer_frame(cv::Mat &frame); // 推理一帧

private:
  cv::dnn::Net net;
  float t_score;
  bool show_fps;
  bool show_score;
};