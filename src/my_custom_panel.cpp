#include "my_custom_panel.hpp"

MyCustomPanel::MyCustomPanel(const bool &id)
{
  std::cout << "Create MyCustomPanel instance ..." << std::endl;
  if (id)
    this->initUIs();
}


void MyCustomPanel::initUIs()
{
  this->setLayout(TEST_BOX_QV());
}


QFont MyCustomPanel::set_font()
{
  QFont font;
  font.setBold(true);     // 设置字体为粗体
  font.setPixelSize(36);  // 设置字体大小为36像素
  return font;
}


QPixmap MyCustomPanel::pix_map()
{
  // 创建一个 QPixmap 对象，加载指定路径的图片
  QPixmap pixmap(QDir::homePath() + "/opencv_tutorial_data/images/gaoyy.png");
  return pixmap; // 返回创建的 QPixmap 对象
}


QPixmap MyCustomPanel::mat_transfer(const std::string &fileName)
{
  cv::Mat bgr;
  // 读取图片文件
  // bgr = cv::imread("/home/ls/Pictures/0f4b8c2e9e94d0215f56003e37e5b9a7.jpg");
  bgr = cv::imread(fileName);

  // 输出图片的宽度和高度
  std::cout << "width:" << bgr.cols << " height:" << bgr.rows << std::endl;

  // 创建一个新的Mat对象，用于存储处理后的图像
  cv::Mat dst;
  // 对原始图像进行模糊处理
  cv::blur(bgr, dst, cv::Size(5, 5));

  // 将处理后的图像从BGR色彩空间转换为RGB色彩空间
  cv::Mat image;
  cv::cvtColor(dst, image, cv::COLOR_BGR2RGB);

  // 将OpenCV的Mat对象转换为Qt的QImage对象
  QImage img = QImage(image.data, image.cols, image.rows, image.step, QImage::Format_RGB888);
  // 对图像进行缩放处理，保持原始比例
  img = img.scaled(QSize(1080, 1080), Qt::KeepAspectRatio);

  // 输出缩放后图像的宽度和高度
  std::cout << "width:" << img.width() << " height:" << img.height() << std::endl;

  // 创建一个空的QPixmap对象
  QPixmap pixmap;
  // 将QImage对象转换为QPixmap对象
  pixmap = pixmap.fromImage(img);
  // 返回转换后的QPixmap对象
  return pixmap;
}


void MyCustomPanel::IMAGE_TEST(const std::string &fileName)
{
  image_label = new QLabel();
  image_label->setAlignment(Qt::AlignCenter); // 当前行中心显示
  // label->setText("Hello World, OpenCV4 + QT5 Application !");
  // label->setPixmap(pix_map());
  image_label->setStyleSheet("background-color: rgb(0, 0, 0);color:red;"); // CSS 样式
  image_label->setPixmap(mat_transfer(fileName));

}


void MyCustomPanel::TEXT_LABEL(const QString &text)
{
  text_label = new QLabel();
  text_label->setFont(set_font());
  text_label->setAlignment(Qt::AlignCenter); // 当前行中心显示
  text_label->setStyleSheet("background-color: rgb(127, 255, 255);color:red;"); // CSS 样式
  text_label->setText(text);
  // label->setPixmap(pix_map());
  // label->setPixmap(mat_transfer());
}


// QVBoxLayout* MyCustomPanel::TEST_BOX_QV()
// {
//   QVBoxLayout *box = new QVBoxLayout();
//   box->addWidget(TEXT_LABEL("Hello World, OpenCV4 + QT5 Application !"));
//   box->addWidget(IMAGE_TEST());
//   box->addWidget(SELECT_BUTTON());
//   return box;
// }


QVBoxLayout* MyCustomPanel::TEST_BOX_QV()
{
  // 初始化垂直布局
  QVBoxLayout *vbox = new QVBoxLayout();
  
  // 添加一个面板到布局中
  vbox->addWidget(PANEL_1());
  
  // 显示一张测试图片
  IMAGE_TEST(QDir::homePath().toStdString() + "/opencv_tutorial_data/images/gaoyy.png");
  
  // 将图片标签添加到布局中
  vbox->addWidget(image_label);
  
  // 在布局底部添加一个伸缩器，使布局具有更好的扩展性
  vbox->addStretch(1);
  
  // 返回配置好的布局
  return vbox;
}


QHBoxLayout* MyCustomPanel::TEST_BOX_QH()
{
  QHBoxLayout *hbox = new QHBoxLayout();
  hbox->addWidget(SELECT_BUTTON());
  TEXT_LABEL("Hello World, OpenCV4 + QT5 Application !");
  hbox->addWidget(text_label);
  // hbox->addWidget(TEXT_LABEL("路生学 OpenCV4 + QT5"));
  // hbox->addWidget(IMAGE_TEST());
  hbox->addStretch(1);

  return hbox;
}


QWidget* MyCustomPanel::PANEL_1()
{
  auto panel = new QWidget();
  panel->setLayout(TEST_BOX_QH());
  return panel;
}


QPushButton* MyCustomPanel::SELECT_BUTTON()
{
  auto selectBtn = new QPushButton();
  selectBtn->setText("Select");
  this->ADD_SELECT_IMAGE_SLOTS(selectBtn);
  return selectBtn;
}


void MyCustomPanel::ADD_SELECT_IMAGE_SLOTS(QPushButton* selectBtn)
{
  // add listener
  connect(selectBtn, SIGNAL(clicked()), this, SLOT(SELECT_IMAGE()));
}

void MyCustomPanel::SELECT_IMAGE()
{
  std::cout<< "button clicked ..." << std::endl;
  auto fileName = QFileDialog::getOpenFileName(this, "Open Image", QDir::homePath() + "/opencv_tutorial_data/images", tr("*.jpg"));
  if (fileName.isEmpty()) return;
  
  this->image_label->setPixmap(mat_transfer(fileName.toStdString()));
}
