#include "radio_checkbox_demo_panel.hpp"

RadioCheckboxDemoPanel::RadioCheckboxDemoPanel(bool id)
    : index(0)
{
  std::cout << "Create RadioCheckboxDemoPanel instance ..." << std::endl;
  if (id)
    this->initUIs();
}

void RadioCheckboxDemoPanel::initUIs()
{
  this->image_label = new QLabel();
  this->text_label = new QLabel();
  this->imgBtn = new QRadioButton("原图");
  this->grayBtn = new QRadioButton("灰度");
  this->gaussBtn = new QRadioButton("高斯");
  this->gradBtn = new QRadioButton("梯度");
  this->show_time_chkbox = new QCheckBox("显示执行时间");
  this->show_algo_chkbox = new QCheckBox("显示算法名称");

  this->setLayout(ORIGIN_BOX());
}

QFont RadioCheckboxDemoPanel::set_font()
{
  QFont font("Arial", 8, QFont::Bold);
  return font;
}

QVBoxLayout *RadioCheckboxDemoPanel::ORIGIN_BOX()
{
  QVBoxLayout *vbox = new QVBoxLayout();

  vbox->addWidget(PANEL_1_());
  vbox->addWidget(PANEL_2_());
  vbox->addWidget(PANEL_3_());
  IMAGE_TEST(QDir::homePath().toStdString() + "/Pictures/微信图片_20230703182903.jpg");
  vbox->addWidget(image_label);

  connect(imgBtn, &QRadioButton::toggled, this, &RadioCheckboxDemoPanel::IMAGE_PROCESS);
  connect(grayBtn, &QRadioButton::toggled, this, &RadioCheckboxDemoPanel::IMAGE_PROCESS);
  connect(gaussBtn, &QRadioButton::toggled, this, &RadioCheckboxDemoPanel::IMAGE_PROCESS);
  connect(gradBtn, &QRadioButton::toggled, this, &RadioCheckboxDemoPanel::IMAGE_PROCESS);
  // vbox->addStretch(1);
  // 返回配置好的布局
  return vbox;
}

QPixmap RadioCheckboxDemoPanel::mat_transfer(const std::string &fileName)
{
  cv::Mat bgr = cv::imread(fileName);
  if (bgr.empty())
  {
    std::cerr << "Error loading image: " << fileName << std::endl;
    return QPixmap();
  }

  std::cout << "width:" << bgr.cols << " height:" << bgr.rows << std::endl;
  cv::Mat dst;
  cv::blur(bgr, dst, cv::Size(5, 5));
  cv::Mat image;
  cv::cvtColor(dst, image, cv::COLOR_BGR2RGB);
  QImage img = QImage(image.data, image.cols, image.rows, image.step, QImage::Format_RGB888);
  img = img.scaled(QSize(800, 600), Qt::KeepAspectRatio);
  std::cout << "width:" << img.width() << " height:" << img.height() << std::endl;

  QPixmap pixmap;
  pixmap = pixmap.fromImage(img);

  return pixmap;
}

void RadioCheckboxDemoPanel::TEXT_LABEL(const QString &text)
{
  text_label->setFont(set_font());
  text_label->setAlignment(Qt::AlignCenter);                                    // 当前行中心显示
  text_label->setStyleSheet("background-color: rgb(127, 255, 255);color:red;"); // CSS 样式
  text_label->setText(text);
}

void RadioCheckboxDemoPanel::IMAGE_TEST(const std::string &fileName)
{
  image_label->setAlignment(Qt::AlignCenter);                              // 当前行中心显示
  image_label->setStyleSheet("background-color: rgb(0, 0, 0);color:red;"); // CSS 样式
  image_label->setPixmap(mat_transfer(fileName));
}

QWidget *RadioCheckboxDemoPanel::PANEL_1_()
{
  QWidget *panel = new QGroupBox("图像选择");
  QHBoxLayout *hbox = new QHBoxLayout();

  hbox->addWidget(SELECT_IMAGE_BUTTON());
  TEXT_LABEL(QDir::homePath() + "/Pictures/微信图片_20230703182903.jpg");
  hbox->addWidget(text_label);
  hbox->addStretch(1);

  panel->setLayout(hbox);

  return panel;
}

QWidget *RadioCheckboxDemoPanel::PANEL_2_()
{
  QWidget *panel = new QGroupBox("算法选择");
  QHBoxLayout *hbox = new QHBoxLayout();

  hbox->addWidget(imgBtn);
  hbox->addWidget(grayBtn);
  hbox->addWidget(gaussBtn);
  hbox->addWidget(gradBtn);
  imgBtn->setChecked(true);
  // hbox->addStretch(1);
  panel->setLayout(hbox);

  return panel;
}

QWidget *RadioCheckboxDemoPanel::PANEL_3_()
{
  QWidget *panel = new QGroupBox("算法选择");
  QHBoxLayout *hbox = new QHBoxLayout();

  hbox->addWidget(show_time_chkbox);
  hbox->addWidget(show_algo_chkbox);
  // hbox->addStretch(1);
  panel->setLayout(hbox);

  return panel;
}

QPushButton *RadioCheckboxDemoPanel::SELECT_IMAGE_BUTTON()
{
  auto selectBtn = new QPushButton();
  selectBtn->setText("Select");
  this->ADD_SELECT_IMAGE_SLOTS(selectBtn);
  return selectBtn;
}

void RadioCheckboxDemoPanel::ADD_SELECT_IMAGE_SLOTS(QPushButton *selectBtn)
{
  connect(selectBtn, &QPushButton::clicked, this, &RadioCheckboxDemoPanel::SELECT_IMAGE);
}

void RadioCheckboxDemoPanel::SELECT_IMAGE()
{
  std::cout << "button clicked ..." << std::endl;
  auto fileName = QFileDialog::getOpenFileName(this, "Open Image", "/home/ls/Pictures", tr("*.jpg *.png"));
  if (fileName.isEmpty())
    return;

  this->text_label->setText(fileName);
  IMAGE_PROCESS();
}

void RadioCheckboxDemoPanel::IMAGE_PROCESS()
{
  std::string file_path = this->text_label->text().toStdString();
  cv::Mat src = cv::imread(file_path);
  if (src.empty())
  {
    std::cerr << "Error loading image: " << file_path << std::endl;
    return;
  }

  cv::Mat bgr;
  int64 st, et;
  float t;
  

  if (this->imgBtn->isChecked())
  {
    src.copyTo(bgr);
  }
  else if (this->grayBtn->isChecked())
  {
    st = cv::getTickCount();
    cv::Mat gray;
    cv::cvtColor(src, gray, cv::COLOR_BGR2GRAY);
    cv::cvtColor(gray, bgr, cv::COLOR_GRAY2BGR);
    et = cv::getTickCount() - st;
    if (this->show_algo_chkbox->isChecked())
      cv::putText(bgr, "Gray:", cv::Point(10, 100), cv::FONT_HERSHEY_PLAIN, 3.0, cv::Scalar(0, 255, 0), 3);
  }
  else if (this->gaussBtn->isChecked())
  {
    st = cv::getTickCount();
    cv::GaussianBlur(src, bgr, cv::Size(0, 0), 15);
    et = cv::getTickCount() - st;
    if (this->show_algo_chkbox->isChecked())
      cv::putText(bgr, "Gauss:", cv::Point(10, 100), cv::FONT_HERSHEY_PLAIN, 3.0, cv::Scalar(0, 255, 0), 3);
  }
  else if (this->gradBtn->isChecked())
  {
    st = cv::getTickCount();
    cv::Sobel(src, bgr, CV_8U, 0, 1); // Use dx = 1, dy = 0 for gradient in the x direction
    et = cv::getTickCount() - st;
    if (this->show_algo_chkbox->isChecked())
      cv::putText(bgr, "Grad:", cv::Point(10, 100), cv::FONT_HERSHEY_PLAIN, 3.0, cv::Scalar(0, 255, 0), 3);
  }

  if (this->show_time_chkbox->isChecked())
  {
    t = et / cv::getTickFrequency();
    cv::putText(bgr, cv::format("%.5f ms", t), cv::Point(200, 100), cv::FONT_HERSHEY_PLAIN, 3.0, cv::Scalar(0, 255, 0), 3);
  }
  cv::Mat image;
  cv::cvtColor(bgr, image, cv::COLOR_BGR2RGB);
  QImage img = QImage(image.data, image.cols, image.rows, image.step, QImage::Format_RGB888);
  img = img.scaled(QSize(800, 600), Qt::KeepAspectRatio);

  this->image_label->setPixmap(QPixmap::fromImage(img));
}
