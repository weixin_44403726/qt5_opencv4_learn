#include "image_browser_panel.hpp"

ImageBrowserPanel::ImageBrowserPanel(const bool &id) 
  : index(0)
{
  std::cout << "Create ImageBrowserPanel instance ..." << std::endl;
  image_label = new QLabel();
  num_text_label = new QLabel();
  file_dir_label = new QLabel();

  if (id)
    this->initUIs();
}

void ImageBrowserPanel::initUIs()
{
  this->setLayout(ORIGIN_BOX());
}

QVBoxLayout *ImageBrowserPanel::ORIGIN_BOX()
{
  // 初始化垂直布局 
  QVBoxLayout *vbox = new QVBoxLayout();

  // 添加一个面板到布局中
  vbox->addWidget(PANEL_1());

  vbox->addWidget(PANEL_2());

  // 显示一张测试图片
  IMAGE_TEST(QDir::homePath().toStdString() + "/Pictures/微信图片_20230703182903.jpg");
  // IMAGE_TEST("/home/ls/Pictures/0f4b8c2e9e94d0215f56003e37e5b9a7.jpg");

  // 将图片标签添加到布局中
  vbox->addWidget(image_label);

  // 在布局底部添加一个伸缩器，使布局具有更好的扩展性
  vbox->addStretch(1);

  // 返回配置好的布局
  return vbox;
}

QFont ImageBrowserPanel::set_font()
{
  QFont font("Arial", 36, QFont::Bold);
  return font;
}

QPixmap ImageBrowserPanel::pix_map()
{
  // 创建一个 QPixmap 对象，加载指定路径的图片
  QPixmap pixmap("/home/ls/Pictures/0f4b8c2e9e94d0215f56003e37e5b9a7.jpg");
  return pixmap; // 返回创建的 QPixmap 对象
}

QPixmap ImageBrowserPanel::mat_transfer(const std::string &fileName)
{
  cv::Mat bgr;
  // 读取图片文件
  // bgr = cv::imread("/home/ls/Pictures/0f4b8c2e9e94d0215f56003e37e5b9a7.jpg");
  bgr = cv::imread(fileName);

  // 输出图片的宽度和高度
  std::cout << "width:" << bgr.cols << " height:" << bgr.rows << std::endl;

  // 创建一个新的Mat对象，用于存储处理后的图像
  cv::Mat dst;
  // 对原始图像进行模糊处理
  cv::blur(bgr, dst, cv::Size(5, 5));

  // 将处理后的图像从BGR色彩空间转换为RGB色彩空间
  cv::Mat image;
  cv::cvtColor(dst, image, cv::COLOR_BGR2RGB);

  // 将OpenCV的Mat对象转换为Qt的QImage对象
  QImage img = QImage(image.data, image.cols, image.rows, image.step, QImage::Format_RGB888);
  // 对图像进行缩放处理，保持原始比例
  img = img.scaled(QSize(600, 500), Qt::KeepAspectRatio);

  // 输出缩放后图像的宽度和高度
  std::cout << "width:" << img.width() << " height:" << img.height() << std::endl;

  // 创建一个空的QPixmap对象
  QPixmap pixmap = QPixmap::fromImage(img);
  // 返回转换后的QPixmap对象
  return pixmap;
}

void ImageBrowserPanel::IMAGE_TEST(const std::string &fileName)
{
  // image_label->setAlignment(Qt::AlignCenter); // 当前行中心显示
  // label->setText("Hello World, OpenCV4 + QT5 Application !");
  // label->setPixmap(pix_map());
  image_label->setStyleSheet("background-color: rgb(0, 0, 0);color:red;"); // CSS 样式
  image_label->setPixmap(mat_transfer(fileName));
}

void ImageBrowserPanel::TEXT_LABEL(const QString &text)
{
  num_text_label->setFont(set_font());
  num_text_label->setAlignment(Qt::AlignCenter);                                    // 当前行中心显示
  num_text_label->setStyleSheet("background-color: rgb(127, 255, 255);color:red;"); // CSS 样式
  num_text_label->setText(text);
}


QWidget *ImageBrowserPanel::PANEL_1()
{
  auto panel = new QGroupBox("图像文件夹信息");
  QHBoxLayout *hbox = new QHBoxLayout();
  hbox->addWidget(SELECT_IMAGE_DIR_BUTTON());
  // TEXT_LABEL("Hello World, OpenCV4 + QT5 Application !");
  hbox->addWidget(file_dir_label);
  hbox->addWidget(num_text_label);
  hbox->addStretch(1);
  panel->setLayout(hbox);
  return panel;
}

QWidget *ImageBrowserPanel::PANEL_2()
{
  auto panel = new QWidget();
  QHBoxLayout *hbox = new QHBoxLayout();
  hbox->addWidget(PREV_IMAGE_BUTTON());
  hbox->addWidget(NEXT_IMAGE_BUTTON());
  // hbox->addWidget(TEXT_LABEL("路生学 OpenCV4 + QT5"));
  // hbox->addWidget(IMAGE_TEST());
  // hbox->addStretch(1);
  panel->setLayout(hbox);
  return panel;
}

QPushButton *ImageBrowserPanel::SELECT_IMAGE_DIR_BUTTON()
{
  auto Btn = new QPushButton();
  Btn->setText("选择图像目录");
  this->ADD_SELECT_IMAGE_DIR_SLOTS(Btn);
  return Btn;
}


QPushButton *ImageBrowserPanel::PREV_IMAGE_BUTTON()
{
  auto icon = QApplication::style()->standardIcon(QStyle::SP_ArrowBack);
  auto Btn = new QPushButton(icon, "上一张");
  // Btn->setText("上一张");
  Btn->setMinimumHeight(50);
  this->ADD_PREV_IMAGE_SLOTS(Btn);
  return Btn;
}


QPushButton *ImageBrowserPanel::NEXT_IMAGE_BUTTON()
{
  auto icon = QApplication::style()->standardIcon(QStyle::SP_ArrowForward);
  auto Btn = new QPushButton(icon, "下一张");
  // Btn->setText("下一张");
  Btn->setMinimumHeight(50);
  this->ADD_NEXT_IMAGE_SLOTS(Btn);
  return Btn;
}


void ImageBrowserPanel::ADD_SELECT_IMAGE_DIR_SLOTS(QPushButton *Btn)
{
  // add listener
  connect(Btn, &QPushButton::clicked, this, &ImageBrowserPanel::SELECT_IMAGE_DIR);
}


void ImageBrowserPanel::ADD_PREV_IMAGE_SLOTS(QPushButton* Btn)
{
  // add listener
  connect(Btn, &QPushButton::clicked, this, &ImageBrowserPanel::PREV_IMAGE);
}


void ImageBrowserPanel::ADD_NEXT_IMAGE_SLOTS(QPushButton* Btn)
{
  // add listener
  connect(Btn, &QPushButton::clicked, this, &ImageBrowserPanel::NEXT_IMAGE);
}


void ImageBrowserPanel::SELECT_IMAGE_DIR()
{
  std::cout << "SELECT_IMAGE_DIR ..." << std::endl;
  auto file_dir = QFileDialog::getExistingDirectory(this, "Select Image Directory", "/home/ls/Pictures");
  if (file_dir.isEmpty()) return;


  this->file_dir_label->setText("目录：" + file_dir);
  cv::glob(file_dir.toStdString(), image_files);
  if (!image_files.size()) return;
  for (auto &image : image_files) {
    std::cout << image << std::endl;
  }

  this->num_text_label->setText("图像总数：" + QString::fromStdString(std::to_string(image_files.size())));
  this->image_label->setPixmap(mat_transfer(image_files[index]));
}

void ImageBrowserPanel::NEXT_IMAGE()
{
  if (image_files.empty()) {
    std::cout << "image_files is empty!" << std::endl;
    return; 
  }
  std::cout << "NEXT_IMAGE ..." << std::endl;
  ++index;
  if (index > image_files.size() - 1) index = 0;
  VIEW_IMAGE();
}


void ImageBrowserPanel::PREV_IMAGE()
{
  if (image_files.empty()) {
    std::cout << "image_files is empty!" << std::endl;
    return; 
  }
  std::cout << "PREV_IMAGE ..." << std::endl;
  --index;
  if (index < 0) index = index > image_files.size() - 1;
  VIEW_IMAGE();
}


void ImageBrowserPanel::VIEW_IMAGE()
{
  this->image_label->setPixmap(mat_transfer(image_files[this->index]));
}
