#include "table_view_panel.hpp"

TableViewPanel::TableViewPanel()
{
  initUIs();
}

void TableViewPanel::initUIs()
{
  // 设置布局
  table_View = new QTableView();

  tableModel = new QStandardItemModel();
  table_View->setModel(tableModel);

  custom_TableModel = new CustomTableModel();
  table_View->setModel(custom_TableModel );
  
  table_View->setMinimumSize(500, 400);                                        // 设置表格视图的最小尺寸
  table_View->setStyleSheet("Qtable_View{background-color:rgb(255,255,255);}"); // 设置表格视图的背景颜色
  table_View->setAlternatingRowColors(true);

  loadDataBtn1 = new QPushButton("数据源1");
  loadDataBtn2 = new QPushButton("数据源2");
  addRecordBtn = new QPushButton("新加一行");
  removeRecordBtn = new QPushButton("删除一行");

  QWidget *panel1 = new QWidget();
  QHBoxLayout *hbox1 = new QHBoxLayout();
  hbox1->addWidget(loadDataBtn1);
  hbox1->addWidget(loadDataBtn2);
  hbox1->addWidget(addRecordBtn);
  hbox1->addWidget(removeRecordBtn);
  panel1->setLayout(hbox1);

  QWidget *panel2 = new QGroupBox("统计数据");
  QVBoxLayout *vbox1 = new QVBoxLayout();
  vbox1->addWidget(table_View);
  panel2->setLayout(vbox1);

  QVBoxLayout *vbox = new QVBoxLayout();
  vbox->addWidget(panel1);
  vbox->addWidget(panel2);
  vbox->addStretch(1);
  this->setLayout(vbox);

  // add listeners
  connect(loadDataBtn1, SIGNAL(clicked()), this, SLOT(dataSource1()));
  connect(loadDataBtn2, SIGNAL(clicked()), this, SLOT(dataSource2()));
  connect(addRecordBtn, SIGNAL(clicked()), this, SLOT(addNewRow()));
  connect(removeRecordBtn, SIGNAL(clicked()), this, SLOT(removeNewRow()));
}

// void TableViewPanel::dataSource1()
// {
//   tableModel->clear();
//   QList<QStandardItem *> row_data;
//   tableModel->setHorizontalHeaderLabels(QStringList() << "姓名" << "年龄" << "性别" << "身高" << "体重");
//   row_data << new QStandardItem("张三")
//            << new QStandardItem("20")
//            << new QStandardItem("男")
//            << new QStandardItem("170")
//            << new QStandardItem("60");


//   tableModel->appendRow(row_data);
//   row_data.clear();
//   row_data << new QStandardItem("李四")
//            << new QStandardItem("21")
//            << new QStandardItem("男")
//            << new QStandardItem("171")
//            << new QStandardItem("61");

//   tableModel->appendRow(row_data);
//   row_data.clear();
//   row_data << new QStandardItem("王五")
//            << new QStandardItem("22")
//            << new QStandardItem("男")
//            << new QStandardItem("172")
//            << new QStandardItem("62");
//   tableModel->appendRow(row_data);
//   row_data.clear();
// }

void TableViewPanel::dataSource1()
{
  custom_TableModel->clear();
  std::vector<std::string> headers = {"姓名", "年龄", "性别", "身高", "体重"};
  custom_TableModel->setMyHeaders(headers);
  std::vector<std::string> row1 = {"张三", "21", "男", "171", "61"};
  custom_TableModel->addMyRow(row1);
  table_View->setModel(nullptr);
  table_View->setModel(custom_TableModel);
}

void TableViewPanel::dataSource2()
{
  // custom_TableModel->clear();
  // std::vector<std::string> headers = {"姓名", "年龄", "性别", "身高", "体重"};
  // custom_TableModel->setMyHeaders(headers);
  std::vector<std::string> row1 = {"李四", "22", "男", "172", "62"};
  custom_TableModel->addMyRow(row1);
  table_View->setModel(nullptr);
  table_View->setModel(custom_TableModel);
}

// void TableViewPanel::dataSource2()
// {
//   tableModel->clear();
//   QList<QStandardItem *> row_data;
//   tableModel->setHorizontalHeaderLabels(QStringList() << "姓名" << "年龄" << "性别" << "身高" << "体重");
//   row_data << new QStandardItem("张三")
//            << new QStandardItem("21")
//            << new QStandardItem("男")
//            << new QStandardItem("171")
//            << new QStandardItem("61");


//   tableModel->appendRow(row_data);
//   row_data.clear();
//   row_data << new QStandardItem("李四")
//            << new QStandardItem("22")
//            << new QStandardItem("男")
//            << new QStandardItem("172")
//            << new QStandardItem("62");

//   tableModel->appendRow(row_data);
//   row_data.clear();
//   row_data << new QStandardItem("王五")
//            << new QStandardItem("23")
//            << new QStandardItem("男")
//            << new QStandardItem("173")
//            << new QStandardItem("63");
//   tableModel->appendRow(row_data);
//   row_data.clear();
// }

void TableViewPanel::addNewRow()
{
  std::vector<std::string> row1 = {"李四", "22", "男", "172", "62"};
  custom_TableModel->addMyRow(row1);
  table_View->setModel(nullptr);
  table_View->setModel(custom_TableModel);
}

void TableViewPanel::removeNewRow()
{
  custom_TableModel->removeMyRow();
  table_View->setModel(nullptr);
  table_View->setModel(custom_TableModel);
}

// void TableViewPanel::removeNewRow()
// {
//   // 删除最后一行
//   tableModel->removeRow(tableModel->rowCount() - 1);
// }
