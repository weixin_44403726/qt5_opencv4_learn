#include "grid_layout_panel.hpp"

GridLayoutPanel::GridLayoutPanel(const bool &id)
    : index(0)
{
  std::cout << "Create GridLayoutPanel instance ..." << std::endl;
  this->image_label = new QLabel();
  this->text_label = new QLabel();

  if (id)
    this->initUIs();
}

void GridLayoutPanel::initUIs()
{
  this->setLayout(ORIGIN_BOX());
}

QVBoxLayout *GridLayoutPanel::ORIGIN_BOX()
{
  // 初始化垂直布局
  QVBoxLayout *vbox = new QVBoxLayout();

  // 添加一个面板到布局中
  vbox->addWidget(PANEL_1());

  vbox->addWidget(PANEL_2());

  // 显示一张测试图片
  // IMAGE_TEST("/home/ls/Pictures/微信图片_20230703182903.jpg");
  // IMAGE_TEST("/home/ls/Pictures/0f4b8c2e9e94d0215f56003e37e5b9a7.jpg");

  // 将图片标签添加到布局中
  // vbox->addWidget(image_label);

  // 在布局底部添加一个伸缩器，使布局具有更好的扩展性
  vbox->addStretch(1);

  // 返回配置好的布局
  return vbox;
}

QFont GridLayoutPanel::set_font()
{
  QFont font("Arial", 18, QFont::Bold);
  return font;
}

QPixmap GridLayoutPanel::pix_map()
{
  // 创建一个 QPixmap 对象，加载指定路径的图片
  QPixmap pixmap(QDir::homePath() + "/opencv_tutorial_data/images/gaoyy.png");
  return pixmap; // 返回创建的 QPixmap 对象
}

QPixmap GridLayoutPanel::mat_transfer(const std::string &fileName)
{
  cv::Mat bgr;
  // 读取图片文件
  // bgr = cv::imread("/home/ls/Pictures/0f4b8c2e9e94d0215f56003e37e5b9a7.jpg");
  bgr = cv::imread(fileName);

  // 输出图片的宽度和高度
  std::cout << "width:" << bgr.cols << " height:" << bgr.rows << std::endl;

  // 创建一个新的Mat对象，用于存储处理后的图像
  cv::Mat dst;
  // 对原始图像进行模糊处理
  cv::blur(bgr, dst, cv::Size(5, 5));

  // 将处理后的图像从BGR色彩空间转换为RGB色彩空间
  cv::Mat image;
  cv::cvtColor(dst, image, cv::COLOR_BGR2RGB);

  // 将OpenCV的Mat对象转换为Qt的QImage对象
  QImage img = QImage(image.data, image.cols, image.rows, image.step, QImage::Format_RGB888);
  // 对图像进行缩放处理，保持原始比例
  img = img.scaled(QSize(600, 500), Qt::KeepAspectRatio);

  // 输出缩放后图像的宽度和高度
  std::cout << "width:" << img.width() << " height:" << img.height() << std::endl;

  // 创建一个空的QPixmap对象
  QPixmap pixmap = QPixmap::fromImage(img);
  // 返回转换后的QPixmap对象
  return pixmap;
}

void GridLayoutPanel::IMAGE_TEST(const std::string &fileName)
{
  // image_label->setAlignment(Qt::AlignCenter); // 当前行中心显示
  // label->setText("Hello World, OpenCV4 + QT5 Application !");
  // label->setPixmap(pix_map());
  image_label->setStyleSheet("background-color: rgb(0, 0, 0);color:red;"); // CSS 样式
  image_label->setPixmap(mat_transfer(fileName));
}

void GridLayoutPanel::TEXT_LABEL(const QString &text)
{
  text_label->setFont(set_font());
  text_label->setAlignment(Qt::AlignCenter);                                    // 当前行中心显示
  text_label->setStyleSheet("background-color: rgb(127, 255, 255);color:red;"); // CSS 样式
  text_label->setText(text);
}

QWidget *GridLayoutPanel::PANEL_1()
{
  auto panel = new QGroupBox();
  QHBoxLayout *hbox = new QHBoxLayout();
  hbox->addWidget(SELECT_IMAGE_DIR_BUTTON());
  TEXT_LABEL("Hello World, OpenCV4 + QT5 Application !");
  hbox->addWidget(text_label);
  hbox->addStretch(1);
  panel->setLayout(hbox);
  return panel;
}

QWidget *GridLayoutPanel::PANEL_2()
{
  auto grid = new QGridLayout();
  QWidget *panel = new QWidget();
  for (int i = 0; i < 9; ++i)
  {
    auto label = new QLabel();
    label->setStyleSheet("background-color: rgb(0, 0, 0);color:red;"); // CSS 样式
    label->setFixedSize(225, 225);
    this->image_labels.emplace_back(label);
    grid->addWidget(label, i / 3, i % 3);
  }
  panel->setLayout(grid);
  return panel;
}

QPushButton *GridLayoutPanel::SELECT_IMAGE_DIR_BUTTON()
{
  auto Btn = new QPushButton();
  Btn->setText("选择图像目录");
  this->ADD_SELECT_IMAGE_DIR_SLOTS(Btn);
  return Btn;
}

void GridLayoutPanel::ADD_SELECT_IMAGE_DIR_SLOTS(QPushButton *Btn)
{
  // add listener
  connect(Btn, &QPushButton::clicked, this, &GridLayoutPanel::SELECT_IMAGE_DIR);
}

void GridLayoutPanel::SELECT_IMAGE_DIR()
{
  std::cout << "SELECT_IMAGE_DIR ..." << std::endl;
  auto file_dir = QFileDialog::getExistingDirectory(this, "Select Image Directory", QDir::homePath() + "/opencv_tutorial_data/images");
  if (file_dir.isEmpty())
    return;

  this->text_label->setText("目录：" + file_dir);
  std::vector<std::string> image_files;
  cv::glob(file_dir.toStdString(), image_files);
  if (!image_files.size())
    return;
  for (auto &image : image_files)
  {
    std::cout << image << std::endl;
  }

  for (int i = 0; i < image_files.size(); ++i)
  {
    if (i >= 9) {
      break;
    }
    this->image_labels[i]->setPixmap(this->mat_transfer(image_files[i]));
  }

}
