#include "resource_dialog_panel.hpp"

ResourceDialogPanel::ResourceDialogPanel()
{
  initUIs();
}

void ResourceDialogPanel::initUIs()
{
  this->image_label = new QLabel();
  image_label->setAlignment(Qt::AlignCenter);                              // 当前行中心显示
  image_label->setStyleSheet("background-color: rgb(0, 0, 0);color:red;"); // CSS 样式
  image_label->setPixmap(QPixmap(QDir::homePath() + "/opencv_tutorial_data/images/llk_tpl.png"));

  // this->text_label = new QLabel();
  // QFont font;
  // font.setBold(true);
  // font.setPixelSize(36);
  // this->text_label->setFont(font);
  // this->text_label->setAlignment(Qt::AlignCenter);


  QPushButton *imput_text_btn = new QPushButton("输入文本...");
  QPushButton *font_select_btn = new QPushButton("字体选择...");
  QPushButton *color_select_btn = new QPushButton("颜色设置...");
  QPushButton *file_select_btn = new QPushButton("文件选择...");
  QPushButton *save_file_btn = new QPushButton("文件保存...");
  QPushButton *dir_select_btn = new QPushButton("目录选择...");

  QWidget *panel_1 = new QGroupBox("字体颜色输入对话框");

  QHBoxLayout *hbox_1 = new QHBoxLayout();
  hbox_1->addWidget(imput_text_btn);
  hbox_1->addWidget(font_select_btn);
  hbox_1->addWidget(color_select_btn);
  
  panel_1->setLayout(hbox_1);

  QWidget *panel_2 = new QGroupBox("文件对话框");
  QHBoxLayout *hbox_2 = new QHBoxLayout();
  hbox_2->addWidget(file_select_btn);
  hbox_2->addWidget(save_file_btn);
  hbox_2->addWidget(dir_select_btn);

  panel_2->setLayout(hbox_2);

  QVBoxLayout *vbox = new QVBoxLayout();
  vbox->addWidget(panel_1);
  vbox->addWidget(panel_2);
  vbox->addWidget(image_label);
  vbox->addStretch(1);
  // vbox->addWidget(text_label);
  this->setLayout(vbox);

  // add listener
  connect(imput_text_btn, &QPushButton::clicked, this, &ResourceDialogPanel::openInputText);
  connect(font_select_btn, &QPushButton::clicked, this, &ResourceDialogPanel::selectFont);
  connect(color_select_btn, &QPushButton::clicked, this, &ResourceDialogPanel::selectColor);
  connect(file_select_btn, &QPushButton::clicked, this, &ResourceDialogPanel::selectImage);
  connect(save_file_btn, &QPushButton::clicked, this, &ResourceDialogPanel::saveFile);
  connect(dir_select_btn, &QPushButton::clicked, this, &ResourceDialogPanel::selectDir);
}

void ResourceDialogPanel::selectImage()
{
  std::cout << "selectImage button clicked ..." << std::endl;
  auto fileName = QFileDialog::getOpenFileName(this, "选择图片", QDir::homePath() + "/opencv_tutorial_data/images", "Image Files(*.png *.jpg *.jpeg *.bmp)");
  if (fileName.isEmpty()) return;
  cv::Mat bgr = cv::imread(fileName.toStdString());
  std::cout << "width:" << bgr.cols << "height:" << bgr.rows << std::endl;
  cv::Mat image;
  cv::cvtColor(bgr, image, cv::COLOR_BGR2RGB);
  QImage img = QImage((const unsigned char *)(image.data), image.cols, image.rows, QImage::Format_RGB888);
  img = img.scaled(800, 600, Qt::KeepAspectRatio);

  QPixmap pixmap;
  pixmap.convertFromImage(img);
  this->image_label->setPixmap(pixmap);
}

void ResourceDialogPanel::selectFont()
{
  bool ok;
  QFont font = QFontDialog::getFont(&ok, this);
  if (ok) {
    this->image_label->setFont(font);
    this->image_label->setText("字体选择...");
  }
}


void ResourceDialogPanel::selectColor()
{
  QColor clr = QColorDialog::getColor(Qt::red, this);
  int r = clr.red();
  int g = clr.green();
  int b = clr.blue();
  this->image_label->setStyleSheet(QString("background-color: rgb(%1, %2, %3);").arg(r).arg(g).arg(b));
}

void ResourceDialogPanel::openInputText()
{
  bool ok;
  auto name = QInputDialog::getText(this, "输入文本", "请输入文本", QLineEdit::Normal, QDir::home().dirName(), &ok);
  std::cout << name.toStdString() << std::endl;
  this->image_label->setText(name);
}

void ResourceDialogPanel::selectDir()
{
  std::cout << "selectDir ..." << std::endl;
  auto file_dir = QFileDialog::getExistingDirectory(this, "Select Image Directory", QDir::homePath() + "/opencv_tutorial_data/images");
  if (file_dir.isEmpty()) return;


  this->image_label->setText("目录：" + file_dir);
}

void ResourceDialogPanel::saveFile()
{
  auto fileName = QFileDialog::getSaveFileName(this, "Save File", QDir::homePath() + "/opencv_tutorial_data/images", "Image Files (*.png *.jpg *.bmp)");
  if(fileName.isEmpty()) return;

  QMessageBox::information(this, "信息", "文件保存成功...");
}
