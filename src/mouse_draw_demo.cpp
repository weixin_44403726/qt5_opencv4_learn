#include "mouse_draw_demo.hpp"
#include <iostream>
#include "QVBoxLayout"
#include "QHBoxLayout"
#include "QPushButton"
#include "QGroupBox"
#include "QFileDialog"

MouseDrawDemo::MouseDrawDemo()
{
  std::cout << "create MouseDrawDemo instance..." << std::endl;
  initUIs();
}

void MouseDrawDemo::initUIs()
{

  lineRbt = new QRadioButton("线段");
  boxRbt = new QRadioButton("矩形");
  circleRbt = new QRadioButton("圆形");
  lineRbt->setChecked(true);

  QHBoxLayout *hbox = new QHBoxLayout();
  QWidget *panel1 = new QGroupBox("绘制类型");
  hbox->addWidget(lineRbt);
  hbox->addWidget(boxRbt);
  hbox->addWidget(circleRbt);
  hbox->addStretch(1);
  panel1->setLayout(hbox);

  mouser_painter_demo_ = new MousePainterDemo();
  QPixmap PixMap(QDir::homePath() + "/opencv_tutorial_data/images/llk_tpl.png");
  mouser_painter_demo_->setImage(PixMap);
  QHBoxLayout *hbox1 = new QHBoxLayout();
  QWidget *panel2 = new QGroupBox("画板");
  hbox1->addWidget(mouser_painter_demo_);
  panel2->setLayout(hbox1);

  QVBoxLayout *vbox = new QVBoxLayout();
  vbox->addWidget(panel1);
  vbox->addWidget(panel2);
  vbox->addStretch(1);
  this->setLayout(vbox);

  // add listener
  connect(lineRbt, SIGNAL(toggled(bool)), this, SLOT(setDrawType()));
  connect(boxRbt, SIGNAL(toggled(bool)), this, SLOT(setDrawType()));
  connect(circleRbt, SIGNAL(toggled(bool)), this, SLOT(setDrawType()));
}

void MouseDrawDemo::setDrawType()
{
  if (lineRbt->isChecked())
  {
    mouser_painter_demo_->setDrawType(0);
  }
  else if (boxRbt->isChecked())
  {
    mouser_painter_demo_->setDrawType(1);
  }
  else if (circleRbt->isChecked())
  {
    mouser_painter_demo_->setDrawType(2);
  }
}
