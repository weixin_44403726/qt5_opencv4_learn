#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "my_custom_panel.hpp"
#include "image_browser_panel.hpp"
#include "grid_layout_panel.hpp"
#include "text_demo_panel.hpp"
#include "radio_checkbox_demo_panel.hpp"
#include "spin_box_demo_panel.hpp"
#include "face_detect_panel.hpp"
#include "resource_dialog_panel.hpp"
#include "table_view_panel.hpp"
#include "custom_image_canvas.hpp"
#include "mouse_painter_demo.hpp"
#include "mouse_draw_demo.hpp"
#include "keyboard_event_panel.hpp"

QT_BEGIN_NAMESPACE
namespace Ui
{
  class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void about_info_show();
  void go_mouse_demo();

private:
  Ui::MainWindow *ui;
  void loadPanel_1_(bool param);
  void loadPanel_2_(bool param);
  void loadPanel_3_(bool param);
  void loadPanel_4_(bool param);
  void loadPanel_5_(bool param);
  void loadPanel_6_(bool param);
  void loadPanel_7_(bool param);
  void loadPanel_8_();
  void loadPanel_9_();
  void loadPanel_10_();

  QTabWidget* tabPanel;

};
#endif // MAINWINDOW_H
