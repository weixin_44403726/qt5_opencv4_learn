#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QToolBar>

// 学习了 Qt 的布局，布局可以设置水平布局和垂直布局，水平布局是默认的，垂直布局是靠左显示的
// 设置文本，设置图像，设置OpenCV 转 Qt
// 设置背景
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
  // loadPanel_1_(true);
  // loadPanel_2_(true);
  // loadPanel_3_(true);
  // loadPanel_4_(true);
  // loadPanel_5_(true);
  // loadPanel_6_(true);
  // loadPanel_7_(true);
  // loadPanel_8_();
  loadPanel_9_();
  // void loadPanel_10_();
}
MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::about_info_show()
{
  QMessageBox::about(this, "作者信息", "路生学Qt");
}

void MainWindow::go_mouse_demo()
{
  // int num = tabPanel->count();
  tabPanel->setCurrentIndex(0);
}

void MainWindow::loadPanel_1_(bool param)
{
  // auto my_custom_panel = new MyCustomPanel(param);
  ui->setupUi(this);
  auto my_custom_panel = new MyCustomPanel(param);
  // 设置中央小部件的布局为垂直布局(默认是靠右显示)

  if (param)
  {
    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(my_custom_panel);
    hbox->addStretch(1);
    ui->centralwidget->setLayout(hbox);
  }
  else
  {
    ui->centralwidget->setLayout(my_custom_panel->TEST_BOX_QV());
  }
  // ui->centralwidget->setLayout(my_custom_panel->TEST_BOX_QH());

  // 窗口标题
  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_2_(bool param)
{
  ui->setupUi(this);
  auto image_browser_panel = new ImageBrowserPanel(param);
  // 设置中央小部件的布局为垂直布局(默认是靠右显示)

  if (param)
  {
    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(image_browser_panel);
    hbox->addStretch(1);
    ui->centralwidget->setLayout(hbox);
  }
  else
  {
    ui->centralwidget->setLayout(image_browser_panel->ORIGIN_BOX());
  }
  // ui->centralwidget->setLayout(my_custom_panel->TEST_BOX_QH());

  // 窗口标题
  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_3_(bool param)
{
  ui->setupUi(this);
  auto grid_layout_panel = new GridLayoutPanel(param);
  // 设置中央小部件的布局为垂直布局(默认是靠右显示)

  if (param)
  {
    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(grid_layout_panel);
    ui->centralwidget->setLayout(hbox);
    // hbox->addStretch(1);
  }
  else
  {
    ui->centralwidget->setLayout(grid_layout_panel->ORIGIN_BOX());
  }
  // ui->centralwidget->setLayout(my_custom_panel->TEST_BOX_QH());

  // 窗口标题
  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_4_(bool param)
{
  ui->setupUi(this);
  auto text_demo_panel = new TextDemoPanel(param);
  // 设置中央小部件的布局为垂直布局(默认是靠右显示)

  if (param)
  {
    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(text_demo_panel);
    ui->centralwidget->setLayout(hbox);
    // hbox->addStretch(1);
  }
  else
  {
    ui->centralwidget->setLayout(text_demo_panel->ORIGIN_BOX());
  }
  // ui->centralwidget->setLayout(my_custom_panel->TEST_BOX_QH());

  // 窗口标题
  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_5_(bool param)
{
  ui->setupUi(this);
  auto radio_checkbox_demo_panel = new RadioCheckboxDemoPanel(param);
  // 设置中央小部件的布局为垂直布局(默认是靠右显示)

  if (param)
  {
    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(radio_checkbox_demo_panel);
    ui->centralwidget->setLayout(hbox);
    // hbox->addStretch(1);
  }
  else
  {
    ui->centralwidget->setLayout(radio_checkbox_demo_panel->ORIGIN_BOX());
  }
  // ui->centralwidget->setLayout(my_custom_panel->TEST_BOX_QH());

  // 窗口标题
  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_6_(bool param)
{
  ui->setupUi(this);
  auto spin_box_demo_panel = new SpinBoxDemoPanel(param);
  // 设置中央小部件的布局为垂直布局(默认是靠右显示)

  if (param)
  {
    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(spin_box_demo_panel);
    ui->centralwidget->setLayout(hbox);
    // hbox->addStretch(1);
  }
  else
  {
    ui->centralwidget->setLayout(spin_box_demo_panel->ORIGIN_BOX());
  }
  // ui->centralwidget->setLayout(my_custom_panel->TEST_BOX_QH());

  // 窗口标题
  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_7_(bool param)
{
  ui->setupUi(this);
  auto face_detect_panel = new FaceDetectPanel(param);
  // 设置中央小部件的布局为垂直布局(默认是靠右显示)

  if (param)
  {
    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(face_detect_panel);
    ui->centralwidget->setLayout(hbox);
    // hbox->addStretch(1);
  }
  else
  {
    ui->centralwidget->setLayout(face_detect_panel->ORIGIN_BOX());
  }
  // ui->centralwidget->setLayout(my_custom_panel->TEST_BOX_QH());

  // 窗口标题
  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_8_()
{
  ui->setupUi(this);
  auto resource_dialog_panel = new ResourceDialogPanel();
  // 设置中央小部件的布局为垂直布局(默认是靠右显示)
  QHBoxLayout *hbox = new QHBoxLayout();
  hbox->addWidget(resource_dialog_panel);
  ui->centralwidget->setLayout(hbox);

  // 窗口标题
  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_9_()
{
  ui->setupUi(this);

  auto my_custom_panel = new MyCustomPanel(true);
  auto grid_layout_panel = new GridLayoutPanel(true);
  auto text_demo_panel = new TextDemoPanel(true);
  auto face_detect_panel = new FaceDetectPanel(true);
  auto spin_box_demo_panel = new SpinBoxDemoPanel(true);
  auto resource_dialog_panel = new ResourceDialogPanel();
  auto table_view_panel = new TableViewPanel();
  auto custom_image_canvas_panel = new CustomImageCanvas();
  auto mouse_painter_demo_panel = new MousePainterDemo();
  auto mouse_draw_demo_panel = new MouseDrawDemo();
  auto key_panel = new KeyBoardEventPanel();

  tabPanel = new QTabWidget();
  tabPanel->addTab(key_panel, "键盘控制");
  tabPanel->addTab(mouse_draw_demo_panel, "鼠标图形绘制");
  tabPanel->addTab(mouse_painter_demo_panel, "鼠标绘制演示");
  tabPanel->addTab(custom_image_canvas_panel, "绘制演示");
  tabPanel->addTab(table_view_panel, "表格演示");
  tabPanel->addTab(face_detect_panel, "FaceDetectPanel");
  tabPanel->addTab(my_custom_panel, "MyCustomPanel");
  tabPanel->addTab(grid_layout_panel, "GridLayoutPanel");
  tabPanel->addTab(text_demo_panel, "TextDemoPanel");
  tabPanel->addTab(spin_box_demo_panel, "SpinBoxDemoPanel");
  tabPanel->addTab(resource_dialog_panel, "ResourceDialogPanel");

  QPixmap PixMap(QDir::homePath() + "/opencv_tutorial_data/images/llk_tpl.png");
  custom_image_canvas_panel->setImage(PixMap);

  PixMap = QDir::homePath() + "/opencv_tutorial_data/images/lena.png";
  mouse_painter_demo_panel->setImage(PixMap);

  QMenuBar* menuBar = new QMenuBar();
  this->setMenuBar(menuBar);
  QMenu* help_menu = new QMenu(tr("帮助"), this);

  QAction* aboutAction = new QAction(tr("关于"), this);
  connect(aboutAction, SIGNAL(triggered()), this, SLOT(about_info_show()));
  help_menu->addAction(aboutAction);

  QAction* mouseAction = new QAction(tr("鼠标绘制界面"), this);
  connect(mouseAction, SIGNAL(triggered()), this, SLOT(go_mouse_demo()));
  help_menu->addAction(mouseAction);

  menuBar->addMenu(help_menu);


  QToolBar* toolBar = new QToolBar("工具栏");
  this->addToolBar(toolBar);

  QAction* iconAboutAction = new QAction(QIcon(QDir::homePath() + "/Qt/Project/qt4_opencv5_learn/image/help-icon-png-16.jpg"), tr("关于"));
  iconAboutAction->setToolTip("显示作者信息");
  connect(iconAboutAction, SIGNAL(triggered()), this, SLOT(about_info_show()));

  toolBar->addAction(iconAboutAction);



  QHBoxLayout *hbox = new QHBoxLayout();
  hbox->addWidget(tabPanel);
  ui->centralwidget->setLayout(hbox);

  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}

void MainWindow::loadPanel_10_()
{
  ui->setupUi(this);

  auto table_view_panel = new TableViewPanel();

  QHBoxLayout *hbox = new QHBoxLayout();
  hbox->addWidget(table_view_panel);
  ui->centralwidget->setLayout(hbox);

  this->setWindowTitle("OpenCV4 + QT5 @2024年8月6日");
}
