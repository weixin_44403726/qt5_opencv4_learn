/* 
resource_dialog_panel.hpp
 */
#pragma once

#include <iostream>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QFileDialog>
#include <QApplication>
#include <QBrush>
#include <QStyle>
#include <QGroupBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QRadioButton>
#include <QCheckBox>
#include <QPainter>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QMessageBox>
#include <opencv2/dnn.hpp>
#include <QColorDialog>
#include <QFontDialog>
#include <QInputDialog>


#pragma execution_character_set("utf-8")

class ResourceDialogPanel : public QWidget {
public:
    Q_OBJECT
public:
  ResourceDialogPanel();
  void initUIs();

private slots:
  void selectImage();
  void selectFont();
  void selectColor();
  void openInputText();
  void selectDir();
  void saveFile();

private:
  QLabel* image_label{nullptr};
  QLabel* text_label{nullptr};
};