/* 
text_demo_panel.hpp
 */


#pragma once

#include <iostream>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QFileDialog>
#include <QApplication>
#include <QBrush>
#include <QStyle>
#include <QGroupBox>
#include <QLineEdit>
#include <QTextBrowser>

class TextDemoPanel : public QWidget {
  Q_OBJECT
public:
  TextDemoPanel(const bool &id);
  void initUIs();

  QVBoxLayout *ORIGIN_BOX();

  QFont set_font();

  /**
   * @brief ImageBrowserPanel::TEST_BOX_QV函数的功能是创建一个垂直布局，并进行一些初始化操作。
   *
   * @return QVBoxLayout* 返回配置好的垂直布局对象。
   */

  QWidget *PANEL_1_();

  QPushButton *SELECT_UPDATE_TEXT_BUTTON();

  void ADD_UPDATE_TEXT_SLOTS(QPushButton *Btn);

public slots:
  void UPDATE_TEXT_SLOTS();

private:

  int index;

  QLineEdit* text_edit{nullptr};
  QTextBrowser* text_browser{nullptr};
};
