/*
mouse_draw_demo.hpp
 */
#pragma once

#include <QWidget>
#include <QRadioButton>
#include "mouse_painter_demo.hpp"

#pragma execution_character_set("utf-8")

class MouseDrawDemo : public QWidget
{
  Q_OBJECT
public:
  MouseDrawDemo();
  void initUIs();

public slots:
  void setDrawType();

private:
  MousePainterDemo *mouser_painter_demo_;

  QRadioButton *lineRbt;
  QRadioButton *boxRbt;
  QRadioButton *circleRbt;
};
