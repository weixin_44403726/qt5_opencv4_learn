/*
my_custom_panel.hpp
 */
#pragma once
#include <iostream>
#include <string>

#include <opencv2/opencv.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QFileDialog>
#pragma execution_character_set("utf-8")

class MyCustomPanel : public QWidget
{
  Q_OBJECT
public:
  MyCustomPanel(const bool &id);
  void initUIs();

  /**
   * @brief 设置字体
   *
   * @return QFont 字体对象
   */
  QFont set_font();

  /**
   * @brief 创建一个 QPixmap 对象
   *
   * @return QPixmap QPixmap 对象
   */
  QPixmap pix_map();

  /**
   * @brief 将 OpenCV 的 cv::Mat 转换为 QPixmap 对象
   *
   * @return QPixmap QPixmap 对象
   */
  QPixmap mat_transfer(const std::string &fileName);


  void IMAGE_TEST(const std::string &fileName);
  void TEXT_LABEL(const QString &text);


  /**
   * @brief MyCustomPanel::TEST_BOX_QV函数的功能是创建一个垂直布局，并进行一些初始化操作。
   * 
   * @return QVBoxLayout* 返回配置好的垂直布局对象。
   */
  QVBoxLayout* TEST_BOX_QV();


  QHBoxLayout* TEST_BOX_QH();

  QWidget* PANEL_1();

  QPushButton* SELECT_BUTTON();
  
  void ADD_SELECT_IMAGE_SLOTS(QPushButton* selectBtn);

public slots:
  void SELECT_IMAGE();

private:
  QLabel *image_label{nullptr};
  QLabel *text_label{nullptr};

};