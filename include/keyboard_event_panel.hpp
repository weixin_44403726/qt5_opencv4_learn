/*
keyboard_event_panel.hpp
 */

#pragma once

#include <QWidget>
#include <QBitmap>

class KeyBoardEventPanel : public QWidget
{
  Q_OBJECT
public:
  explicit KeyBoardEventPanel();
  void paintEvent(QPaintEvent *event) override;
  void keyPressEvent(QKeyEvent *event) override;
  void keyReleaseEvent(QKeyEvent *event) override;

private:
  QString textInfo = QString::fromLocal8Bit("路生学Qt");
  int cx = 400;
  int cy = 300;
  int radius = 50;
};