/* 
grid_layout_panel.hpp
 */


#pragma once

// #include "image_browser_panel.hpp"
#include <iostream>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QFileDialog>
#include <QApplication>
#include <QBrush>
#include <QStyle>
#include <QGroupBox>

class GridLayoutPanel : public QWidget {
  Q_OBJECT
public:
  GridLayoutPanel(const bool &id);
  void initUIs();

  QVBoxLayout *ORIGIN_BOX();

  /**
   * @brief 设置字体
   *
   * @return QFont 字体对象
   */
  QFont set_font();

  /**
   * @brief 创建一个 QPixmap 对象
   *
   * @return QPixmap QPixmap 对象
   */
  QPixmap pix_map();

  /**
   * @brief 将 OpenCV 的 cv::Mat 转换为 QPixmap 对象
   *
   * @return QPixmap QPixmap 对象
   */
  QPixmap mat_transfer(const std::string &fileName);

  void IMAGE_TEST(const std::string &fileName);
  void TEXT_LABEL(const QString &text);

  /**
   * @brief ImageBrowserPanel::TEST_BOX_QV函数的功能是创建一个垂直布局，并进行一些初始化操作。
   *
   * @return QVBoxLayout* 返回配置好的垂直布局对象。
   */

  QWidget *PANEL_1();
  QWidget *PANEL_2();

  QPushButton *SELECT_IMAGE_DIR_BUTTON();

  void ADD_SELECT_IMAGE_DIR_SLOTS(QPushButton *Btn);

public slots:
  void SELECT_IMAGE_DIR();

private:

  int index;

  QLabel* image_label{nullptr};
  QLabel* text_label{nullptr};
  std::vector<QLabel*> image_labels; // 3x3
};
