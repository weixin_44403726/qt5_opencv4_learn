/* 
radio_checkbox_demo_panel.hpp
 */


#pragma once

#include <iostream>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QFileDialog>
#include <QApplication>
#include <QBrush>
#include <QStyle>
#include <QGroupBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QRadioButton>
#include <QCheckBox>
#include <QPainter>

#pragma execution_character_set("utf-8")

class RadioCheckboxDemoPanel : public QWidget {
  Q_OBJECT
public:
  RadioCheckboxDemoPanel(const bool id);
  void initUIs();

  QVBoxLayout *ORIGIN_BOX();

  QFont set_font();

  QPixmap mat_transfer(const std::string &fileName);

  void TEXT_LABEL(const QString &text);
  void IMAGE_TEST(const std::string &fileName);

  QWidget *PANEL_1_();
  QWidget *PANEL_2_();
  QWidget *PANEL_3_();

  QPushButton* SELECT_IMAGE_BUTTON();
  
  void ADD_SELECT_IMAGE_SLOTS(QPushButton* selectBtn);

public slots:
  void SELECT_IMAGE();
  void IMAGE_PROCESS();

private:

  int index;

  QRadioButton* imgBtn{nullptr};
  QRadioButton* grayBtn{nullptr};
  QRadioButton* gaussBtn{nullptr};
  QRadioButton* gradBtn{nullptr};

  QCheckBox* show_time_chkbox{nullptr};
  QCheckBox* show_algo_chkbox{nullptr};

  QLabel *image_label{nullptr};
  QLabel *text_label{nullptr};
};
