/* 
custom_image_canvas.hpp
 */
#pragma once
#include <QWidget>
#include <QBitmap>

#pragma execution_character_set("utf-8")

class CustomImageCanvas : public QWidget {
  Q_OBJECT
public:
  explicit CustomImageCanvas();

  void paintEvent(QPaintEvent *event) override;
  void setTextInfo(const QString &text);
  void setImage(const QPixmap &image);

private:
  QString text_info_ = QString::fromLocal8Bit("路生学Qt"); // 中文显示必须以该方法转换否则显示错误
  QPixmap pix_map_;
};