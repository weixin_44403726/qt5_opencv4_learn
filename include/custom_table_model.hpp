/*
custom_table_model.hpp
 */
#pragma once
#include <iostream>
#include <QModelIndex>
#include <QAbstractTableModel>
#include <QVariant>
#include <QBrush>

class CustomTableModel : public QAbstractTableModel
{
  Q_OBJECT
public:
  explicit CustomTableModel(QObject *parent = nullptr);

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  void clear();

  void setMyHeaders(std::vector<std::string> &headers);
  void addMyRow(std::vector<std::string> &row_data);
  void removeMyRow();

private:
  std::vector<std::string> header_titles;
  std::vector<std::vector<std::string>> table_data;
};