/* 
face_detect_panel.hpp
 */
#pragma once

#include <iostream>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QFileDialog>
#include <QApplication>
#include <QBrush>
#include <QStyle>
#include <QGroupBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QRadioButton>
#include <QCheckBox>
#include <QPainter>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QMessageBox>
#include <opencv2/dnn.hpp>
#include <QCamera>
#include <QCameraInfo>
#include <QProcess>



#pragma execution_character_set("utf-8")

class FaceDetectPanel : public QWidget {
  Q_OBJECT
public:
  FaceDetectPanel(const bool id);
  void initUIs();

  QVBoxLayout *ORIGIN_BOX();

  QFont set_font();

  QPixmap mat_transfer(const std::string &fileName);

  QWidget *PANEL_1_();
  QWidget *PANEL_2_();
  QWidget *PANEL_3_();
  QWidget *PANEL_4_();

  QPushButton* SELECT_FILE_BUTTON();
  QPushButton* SELECT_WEIGHT_FILE_BUTTON();
  QPushButton* SELECT_CONFIG_FILE_BUTTON();
  QPushButton* DO_FACE_DETECT_BUTTON();
  
  void ADD_SELECT_FILE_SLOTS(QPushButton* selectBtn);
  void ADD_SELECT_WEIGHT_FILE_SLOTS(QPushButton *weightBtn);
  void ADD_SELECT_CONFIG_FILE_SLOTS(QPushButton *configtBtn);
  void ADD_DO_FACE_DETECT_SLOTS(QPushButton *applyBtn);

  void listAvailableCameras();

private:

  int index;
  bool is_camera_running;

  QLabel *image_label{nullptr};
  // 行输入框
  QLineEdit* weight_file_edit{nullptr};
  QLineEdit* config_file_edit{nullptr};
  QLineEdit* data_file_edit{nullptr};
  // 勾选框
  QCheckBox* show_fps_chk{nullptr};
  QCheckBox* show_score_chk{nullptr};  
  // 计数调节器
  QDoubleSpinBox* score_spinbox{nullptr};
  // 点选器
  QRadioButton* img_file_rbt{nullptr};
  QRadioButton* video_file_rbt{nullptr};
  QRadioButton* vision_file_rbt{nullptr};

  QPushButton* disconnect_camera_btn{nullptr};

  void process_frame(cv::Mat &frame, float t_score, bool sfps, bool sScore, cv::dnn::Net &net);
  void dialog_box();

  QComboBox* camera_combo;
  void updateCameraList();

private slots:
  void SELECT_FILE();
  void SELECT_WEIGHT_FILE();
  void SELECT_CONFIG_FILE();
  void DO_FACE_DETECT();
  void DISCONNECT_CAMERA();
  void onCameraSelected(int index);
}; 
