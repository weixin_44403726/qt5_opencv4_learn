/* 
spin_box_demo_panel.hpp
 */
#pragma once

#include <iostream>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QFileDialog>
#include <QApplication>
#include <QBrush>
#include <QStyle>
#include <QGroupBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QRadioButton>
#include <QCheckBox>
#include <QPainter>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>

#pragma execution_character_set("utf-8")

class SpinBoxDemoPanel : public QWidget {
  Q_OBJECT
public:
  SpinBoxDemoPanel(const bool id);
  ~SpinBoxDemoPanel();
  void initUIs();

  QVBoxLayout *ORIGIN_BOX();

  QFont set_font();

  QPixmap mat_transfer(const std::string &fileName);

  void TEXT_LABEL(const QString &text);
  void IMAGE_TEST(const std::string &fileName);

  QWidget *PANEL_1_();
  QWidget *PANEL_2_();
  QWidget *PANEL_3_();

  QPushButton* SELECT_IMAGE_BUTTON();
  QPushButton* DO_DENOISE_PROCESS_BUTTON();
  
  void ADD_SELECT_IMAGE_SLOTS(QPushButton* selectBtn);
  void ADD_DO_DENOISE_PROCESS_SLOTS(QPushButton *applyBtn);

public slots:
  void SELECT_IMAGE();
  void DO_DENOISE_PROCESS();


private:

  int index;

  QLabel *image_label{nullptr};
  QLabel *text_label{nullptr};

  QSpinBox* win_spinbox{nullptr};
  QDoubleSpinBox* sigma_spinbox{nullptr};

  QComboBox* combox{nullptr};
};