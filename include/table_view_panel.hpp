/*
table_view_panel.hpp
 */
#pragma once
#include <iostream>
#include <string>

#include <opencv2/opencv.hpp>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QFileDialog>
#include <QTableView>
#include <QStandardItemModel>
#include <QGroupBox>
#include "custom_table_model.hpp"

#pragma execution_character_set("utf-8")

class TableViewPanel : public QWidget
{
  Q_OBJECT
public:
  TableViewPanel();
  void initUIs();

private slots:
  void dataSource1();
  void dataSource2();
  void addNewRow();
  void removeNewRow();

private:
  QTableView* table_View;
  QStandardItemModel* tableModel;
  CustomTableModel* custom_TableModel;

  QPushButton* loadDataBtn1;
  QPushButton* loadDataBtn2;
  QPushButton* addRecordBtn;
  QPushButton* removeRecordBtn;
};