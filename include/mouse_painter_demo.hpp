/*
mouse_painter_demo.hpp
 */

#pragma once

#include <QWidget>
#include <QPainter>

#pragma execution_character_set("utf-8")

class MousePainterDemo : public QWidget
{
  Q_OBJECT
public:
  explicit MousePainterDemo();

  void paintEvent(QPaintEvent *event) override;
  void setTextInfo(const QString &text);
  void setImage(const QPixmap &image);

  void mousePressEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent *event) override;
  void mouseDoubleClickEvent(QMouseEvent *event) override;
  void mouseMoveEvent(QMouseEvent *event) override;

  void setDrawType(int draw_type);

public slots:
  void changeLine_W_2();
  void changeLine_W_4();
  void changeLine_W_8();

private:
  QString text_info_ = QString::fromLocal8Bit("路生学Qt"); // 中文显示必须以该方法转换否则显示错误
  QPixmap pix_map_;

  QPoint start_point_;
  QPoint end_point_;
  QRectF box;
  QLine line_;
  QPoint center;
  int radius = 0;

  bool is_draw_ = false;
  int draw_type_ = 0; 
  int line_width_ = 2;
};
